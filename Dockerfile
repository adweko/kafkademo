FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y openjdk-8-jdk

ADD . /mnt

RUN cp /mnt/kafka_2.11-2.0.0.tgz /opt/
RUN tar -xzf /opt/kafka_2.11-2.0.0.tgz -C /opt/
RUN cp /mnt/kafka-test-1.0.jar /opt/kafka_2.11-2.0.0/libs/
RUN cp /mnt/initstart.sh /opt/
RUN chmod ugo+x /opt/initstart.sh
RUN cp /mnt/consumer.sh /opt/
RUN chmod ugo+x /opt/consumer.sh

EXPOSE 2181 9092

ENTRYPOINT /opt/initstart.sh
