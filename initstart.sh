#!/bin/bash

TopicNameInput="streams-plaintext-input"
TopicNameOutput="streams-wordcount-output"

echo "Start zookeeper"
/opt/kafka_2.11-2.0.0/bin/zookeeper-server-start.sh -daemon /opt/kafka_2.11-2.0.0/config/zookeeper.properties
sleep 5

echo "Start kafka"
/opt/kafka_2.11-2.0.0/bin/kafka-server-start.sh -daemon /opt/kafka_2.11-2.0.0/config/server.properties
sleep 5

echo "Create topic with name: $TopicNameInput"
/opt/kafka_2.11-2.0.0/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic $TopicNameInput

echo "Create topic with name: $TopicNameOutput"
/opt/kafka_2.11-2.0.0/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic $TopicNameOutput --config cleanup.policy=compact

echo "Start app: WordCountDemo"
java -jar /opt/kafka_2.11-2.0.0/libs/kafka-test-1.0.jar &

echo "Start kafka producer"
/opt/kafka_2.11-2.0.0/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic $TopicNameInput