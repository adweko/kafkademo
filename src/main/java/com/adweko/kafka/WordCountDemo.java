package com.adweko.kafka;


import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Produced;

import java.util.Arrays;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class WordCountDemo {

    public static void main(final String[] args) throws Exception {
        Settings settings = new Settings();
        settings.setDefaultProperties();
        Properties streamsConfiguration = settings.getProperties();

        StreamsBuilder builder = new StreamsBuilder();

        // count all words
        KStream<String, String> source = builder.stream(settings.getTopicNameInput());

        final KTable<String, Long> counts = source
                .flatMapValues(value -> Arrays.asList(value.toLowerCase(Locale.getDefault()).split(" ")))
                .groupBy((key, value) -> value)
                .count();

        counts.toStream().to(settings.getTopicNameOutput(), Produced.with(Serdes.String(), Serdes.Long()));

        // send message one to one to consumer
        //builder.stream(settings.getTopicNameInput()).to(settings.getTopicNameOutput());
        final KafkaStreams streams = new KafkaStreams(builder.build(), streamsConfiguration);
        final CountDownLatch latch = new CountDownLatch(1);

        Runtime.getRuntime().addShutdownHook(new Thread("streams-wordcount-shutdown-hook"){
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (Exception e) {
            System.exit(1);
        }
        System.exit(0);
    }

}
