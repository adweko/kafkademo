package com.adweko.kafka;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import java.util.Properties;

public class Settings {

    private Properties properties;
    private String server;
    private String topicNameInput;
    private String topicNameOutput;

    public Settings() {
        this.properties = new Properties();
    }

    public Properties getProperties() {
        return properties;
    }

    public void addProperties(Object key, Object value){
        this.properties.put(key, value);
    }

    public void setDefaultProperties(){
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "wordcount-lambda-example");
        properties.put(StreamsConfig.CLIENT_ID_CONFIG, "wordcount-lambda-example-client");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        properties.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 10 * 1000);
        properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        topicNameInput = "streams-plaintext-input";
        topicNameOutput = "streams-wordcount-output";
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getTopicNameInput() {
        return topicNameInput;
    }

    public void setTopicNameInput(String topicNameInput) {
        this.topicNameInput = topicNameInput;
    }

    public String getTopicNameOutput() {
        return topicNameOutput;
    }

    public void setTopicNameOutput(String topicNameOutput) {
        this.topicNameOutput = topicNameOutput;
    }
}
