package com.adweko.kafka;

import org.apache.kafka.streams.StreamsConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SettingsTest {

    private Settings settings;

    @BeforeEach
    public void initConstructor(){
        settings = new Settings();
    }

    @Test
    public void testConstructor(){
        assertNotNull(settings.getProperties());
    }

    @Test
    public void testDefaultServer(){
        settings.setDefaultProperties();
        String server = (String) settings.getProperties().get(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG);
        assertEquals("localhost:9092", server);
    }

    @Test
    public void testDefaultTopicNameInput(){
        settings.setDefaultProperties();
        String topicNameInput = settings.getTopicNameInput();
        assertEquals("streams-plaintext-input", topicNameInput);
    }

    @Test
    public void testDefaultTopicNameOutput(){
        settings.setDefaultProperties();
        String topicNameOutput = settings.getTopicNameOutput();
        assertEquals("streams-wordcount-output", topicNameOutput);
    }
}